module.exports = {
    convertMongoosestoOb : function(mongooses) {
        return mongooses.map( item => item.toObject())
    },
    convertMongoosetoOb : function(mongoose) {
        return mongoose.toObject()
    }
}