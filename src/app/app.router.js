const coursesRouter = require('./courses/courses.router.')

function route(app) {
    app.use('/courses',coursesRouter)
}

module.exports = route;
