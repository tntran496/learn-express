const mongoose = require('mongoose')
const mongoose_delete = require('mongoose-delete');
const Schema = mongoose.Schema
const slug = require('mongoose-slug-generator');
mongoose.plugin(slug)

const Course = new Schema({
    name : { type : String, maxLength : 255,require : true},
    description:{ type : String,maxLength : 600},
    image: { type :String,maxLength : 255 },
    slug: {type : String,slug : 'name'},
    videoId : {type : String,require : true},
    level:{ type :String,maxLength : 255 },
},{
    timestamps : true
})

//add plugin
Course.plugin(mongoose_delete,{
    deletedAt : true,
    overrideMethods : 'all'
})

module.exports = mongoose.model('Course',Course)