const Course = require("./courses.model")
const {
    convertMongoosetoOb
} = require('../../utils/mongoose')
const {
    convertMongoosestoOb
} = require('../../utils/mongoose')

class CoursesController {
    index(req, res, next) {
        Course.find({})
            .then(courses => res.render('home', {
                courses: convertMongoosestoOb(courses)
            }))
            .catch(next) //err => next(err)
    }

    show(req, res, next) {
        Course.findOne({
                slug: req.params.slug
            })
            .then(courses => res.render('courses/show', {
                course: convertMongoosetoOb(courses)
            }))
            .catch(next) //err => next(err)
    }

    showMyCourse(req, res, next) {
        Promise.all([Course.find(),Course.countDeleted()])
            .then( ([courses,countCourseDelete]) => {
                res.render('courses/my-list',{
                    countCourseDelete : countCourseDelete,
                    courses : convertMongoosestoOb(courses)
                })
            })
            .catch(next)
    }

    showMyTrash(req, res, next) {
        Course.findDeleted()
            .then(courses => res.render('courses/my-trash', {
                courses: convertMongoosestoOb(courses)
            }))
            .catch(next)
    }

    create(req, res, next) {
        res.render('courses/create')
    }

    store(req, res, next) {
        //res.json(req.Body)

        const formData = {
            ...req.body
        }
        console.log(formData)
        formData.image = `https://img.youtube.com/vi/${formData.videoId}/sddefault.jpg`
        const course = new Course(formData)
        course.save()
            .then(() => res.redirect('/courses'))
            .catch(next)
    }

    edit(req, res, next) {
        Course.findById(req.params.id)
            .then(result => res.render('courses/edit', {
                course: convertMongoosetoOb(result)
            }))
            .catch(next)
    }

    update(req, res, next) {
        // res.send(req.body)
        Course.updateOne({
                _id: req.params.id
            }, req.body)
            .then(() => res.redirect('/courses/me/store'))
            .catch(next)
    }

    destroy(req, res, next) {
        Course.delete({
                _id: req.params.id
            })
            .then(() => res.redirect('back'))
            .catch(next)
    }
    destroyForever(req, res, next) {
        Course.deleteOne({
                _id: req.params.id
            })
            .then(() => res.redirect('back'))
            .catch(next)
    }

    recovery(req,res,next){  
        Course.restore({
            _id: req.params.id
        })
        .then(() => res.redirect('/courses/me/trash'))
        .catch(next)
    }
}

module.exports = new CoursesController