const express = require('express');
const router = express.Router();

const CoursesController = require('./courses.controller');

router.get('/',CoursesController.index)
router.get('/create',CoursesController.create)
router.get('/me/store',CoursesController.showMyCourse)
router.get('/me/trash',CoursesController.showMyTrash)
router.get('/:id/edit',CoursesController.edit)
router.post('/store',CoursesController.store)
router.put('/:id',CoursesController.update)
router.patch('/:id/restore',CoursesController.recovery)
router.delete('/:id',CoursesController.destroy)
router.delete('/:id/forever',CoursesController.destroyForever)
router.get('/:slug', CoursesController.show)

module.exports = router;