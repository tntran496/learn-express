'use-strict';

//import
const express = require('express')
const morgan = require('morgan')
const path = require('path')
const handlebars = require('express-handlebars') 
const app = express()
const port = 3000
const route = require('./app/app.router')
const db = require('./config/db')
const methodOverride = require('method-override')
const format = require('date-format');

//db mongo
db.connect();

// override with the X-HTTP-Method-Override header in the request
app.use(methodOverride('_method'))

// Parse JSON bodies (as sent by API clients)
app.use(express.json());

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded({extended : true}));

//img
app.use(express.static(path.join(__dirname,'public')))

//morgan
app.use(morgan('dev'))

//handlebars
app.engine('hbs',handlebars(
  {
    extname : '.hbs',
    helpers : {
      sum : (a,b) => a + b,
      formatDate: (date) => {
        return format.asString('dd/MM/yyyy', date)
      } 
    }
  }
))
app.set('view engine','hbs')
app.set('views',path.join(__dirname,'resource','views'))

route(app)

//router
app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
})